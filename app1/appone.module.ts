import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppOneComponent } from './appone.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  declarations: [AppOneComponent],
  providers: [AppOneComponent],
  bootstrap: [AppOneComponent],
})
export class AppOneModule { }
