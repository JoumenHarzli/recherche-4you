import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@orchestrator/store';

@Component({
  selector: 'app-one',
  template: `
    <h1>Je suis app 1</h1>
    <form (ngSubmit)="submit(f.value)" #f="ngForm">
      <input type="text" name="msg" ngModel >
      <button type="submit">Envoyer</button>
    </form>
    `
})
export class AppOneComponent {

  submit(values) {
    Store.getInstance().publish("1", values.msg);
    console.log('app1 : ', values.msg);
  }
}
