import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppOneModule } from './appone.module';

export function bootstrap() {
  platformBrowserDynamic().bootstrapModule(AppOneModule);
}

