import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { ApptwoComponent } from './apptwo.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
  ],
  declarations: [ApptwoComponent],
  providers: [ApptwoComponent],
  bootstrap: [ApptwoComponent],
})
export class AppTwoModule { }
