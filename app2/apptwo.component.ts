import { Component } from '@angular/core';
import { Store } from '@orchestrator/store';
import { OnInit, ChangeDetectionStrategy, ApplicationRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-two',
  template: `
    <h1>Je suis app 2</h1>
    <div *ngIf="message">App1 a dit {{ message }}</div>
    <div *ngIf="!message">pas de message recu de app1</div>
    `
})
export class ApptwoComponent implements OnInit {
  message: string;
  ici: string;

  constructor(private ref: ApplicationRef) {
  }

  ngOnInit() {
    Store.getInstance().subscribe("1").subscribe(
      (variable) => {
        console.log('app2 : ', variable);
        this.message = String(variable);
        this.ref.tick();
      });
  }
}
