import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppTwoModule } from './apptwo.module';

export function bootstrap() {
  platformBrowserDynamic().bootstrapModule(AppTwoModule);
}
