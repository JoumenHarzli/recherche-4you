import { Subject } from 'rxjs/Subject';

interface StoreValue extends Subject<Object> { }

interface Content extends Map<String, StoreValue> { }

export class Store {
  private static instance: Store;
  private state$: Content;

  private constructor() {
    this.state$ = new Map<String, StoreValue>();
  }

  public static getInstance(): Store {
    if (this.instance === null || this.instance === undefined) {
      this.instance = new Store();
    }
    return this.instance;
  }

  private keyExist(id: string): boolean {
    return this.state$.has(id);
  }

  private init(id: string, content: Object) {
    this.state$.set(id, new Subject<Object>());
  }

  private push(id: string, content: Object) {
    this.state$.get(id).next(content);
  }

  public publish(id: string, content: Object) {
    if (this.keyExist(id)) {
      this.push(id, content);
    } else {
      this.init(id, content);
    }
  }

  public subscribe(id: string) {
    if (!this.keyExist(id)) {
      this.init(id, new Object());
    }
    return this.state$.get(id).asObservable();
  }

}
