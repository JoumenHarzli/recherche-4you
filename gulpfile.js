const gulp = require('gulp');
const del = require('del');
const tsc = require('gulp-typescript');
const Builder = require('systemjs-builder');
const sourcemaps = require('gulp-sourcemaps');
const gutil = require('gulp-util');
const browserSync = require('browser-sync').create();
const rollup = require('rollup'),
  rollupNodeResolve = require('rollup-plugin-node-resolve'),
  rollupCommonjs = require('rollup-plugin-commonjs');

/**
 *  Suppression des fichiers et des repertoires
 */
function deleteSync(target) {
  return del.sync(target);
}

/**
 * Compilation du typescript
 */
function compileTS(source, destination, done) {
  const tsProject = tsc.createProject('tsconfig.json');
  gulp.src(source)
    .pipe(sourcemaps.init())
    .pipe(tsProject()).js
    .on('error', function (err) {
      gutil.log(gutil.colors.red('[' + title + '] Error'), err.message);
      this.emit('end');
    })
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(destination))
    .on('end', function () {
      done();
    });
}

function bundleWithRollup(mainsrc, bundledest, done) {
  rollup.rollup({
    entry: mainsrc,
    plugins: [
      rollupNodeResolve({ jsnext: true, module: true }),
      rollupCommonjs({
        include: 'node_modules/rxjs/**',
      }),
      //rollupUglify(),
    ],
    external: [
      '@angular/core',
      '@angular/common',
      '@angular/compiler',
      '@angular/core',
      '@angular/http',
      '@angular/platform-browser',
      '@angular/platform-browser-dynamic',
      '@angular/router',
      '@orchestrator/store']
  }).then(function (bundle) {
    bundle.write({
      dest: bundledest,
      sourceMap: false,
      format: 'amd',
      moduleName: 'app1'
    });
    done();
  });
}

function bundleWithRollupUMD(mainsrc, bundledest, done) {
  rollup.rollup({
    entry: mainsrc,
    plugins: [
      rollupNodeResolve({ jsnext: true, module: true }),
      rollupCommonjs({
        include: 'node_modules/rxjs/**',
      }),
      //rollupUglify(),
    ]
  }).then(function (bundle) {
    bundle.write({
      dest: bundledest,
      sourceMap: false,
      format: 'amd',
      moduleName: '@orchestrator/store'
    });
    done();
  });
}

/**
 * Bundle de projet
 */
function bundleDuProjet(source, destination, done) {
  builder = new Builder('./', 'systemjs.config.js');
  builder
    .bundle(source, destination)
    .then(function () {
      gutil.log('Build complete');
      done();
    })
    .catch(function (err) {
      gutil.log('Build error');
      gutil.log(err);
    });
}

/**
 * Lancement du serveur
 */
function launchServer() {
  return browserSync.init({
    port: 8080,
    server: {
      baseDir: './',
      index: 'index.html',
      routes: {
        '/node_modules': 'node_modules',
      },
    },
  });
}

gulp.task('clean', function (done) {
  del.sync(['app1/dist/**/*', 'app2/dist/**/*', 'core/dist/**/*']);
  done();
});

gulp.task('compile:app1', ['compile:core'], function (done) {
  compileTS('app1/**/*.ts', 'app1/dist/', done);
});

gulp.task('compile:app2', ['compile:core'], function (done) {
  compileTS('app2/**/*.ts', 'app2/dist/', done);
});

gulp.task('compile:core', ['clean'], function (done) {
  compileTS('core/**/*.ts', 'core/dist/', done);
});


gulp.task('compile', ['compile:app1', 'compile:app2'], function (done) {
  if (browserSync.active) {
    browserSync.reload();
  }
  done();
});

gulp.task('bundle:app1', ['compile'], function (done) {
  //bundleDuProjet('app1/dist/main.js', 'app1/dist/app1.js', done)
  bundleWithRollup('app1/dist/main.js', 'app1/dist/app1.js', done);
});

gulp.task('bundle:app2', ['compile'], function (done) {
  bundleWithRollup('app2/dist/main.js', 'app2/dist/app2.js', done)
});

gulp.task('bundle:store', ['compile'], function (done) {
  bundleWithRollup('core/dist/store.js', 'core/dist/store.umd.js', done)
});

gulp.task('bundle', ['bundle:app1', 'bundle:app2', 'bundle:store'], function (done) {
  if (browserSync.active) {
    browserSync.reload();
  }
  done();
});

gulp.task('build', ['compile'], function () {
  launchServer();
  //gulp.watch(['app1/**/*', 'app2/**/*', 'index.html', 'tsconfig.json', 'gulpfile.js'], ['compile']);
});

gulp.task('build:prod', ['bundle'], function () {
  launchServer();
  gulp.watch(['app1/**/*.ts', 'app2/**/*.ts', 'core/**/*.ts', 'index.html', 'tsconfig.json', 'gulpfile.js'], ['bundle']);
});
